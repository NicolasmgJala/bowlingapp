﻿using AutoMapper;
using BowlingAPI.DTOs;
using BowlingAPI.Entities;
using System.ComponentModel.DataAnnotations;

namespace BowlingAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {

            CreateMap<Player, PlayerDTO>().ReverseMap();
            CreateMap<CreatePlayerDTO, Player>()
                .ForMember(x => x.Games, Options => Options.MapFrom(MapGames));

            CreateMap<Round, RoundDTO>().ReverseMap();
            CreateMap<CreateRoundDTO, Round>();

            CreateMap<RoundPatchDTO, Round>().ReverseMap();

        }

        private List<Game> MapGames(CreatePlayerDTO createPlayerDTO, Player player)
        {
            var results = new List<Game>();
            if(createPlayerDTO.RoundsIDs == null) { return results; }
            foreach (var id in createPlayerDTO.RoundsIDs)
            {
                results.Add(new Game() { RoundId = id });
            }
            return results;
        }
    }
    
}

