﻿using Microsoft.EntityFrameworkCore;

namespace BowlingAPI.Helpers
{
    public static class HttpContextExtensions
    {
        public async static Task PagesParameters<T>(this HttpContext httpContext, 
            IQueryable<T> queryable, 
            int RegistersPerPage)
        {
            double amount = await queryable.CountAsync();
            double amountPages = Math.Ceiling(amount/RegistersPerPage);
            httpContext.Response.Headers.Add("amountPages", amountPages.ToString());
        }
    }
}
