﻿using BowlingAPI.DTOs;

namespace BowlingAPI.Helpers
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> Pagine<T>(this IQueryable<T> queryable, PaginationDTO paginationDTO)
        {
            return queryable
                .Skip((paginationDTO.Page - 1) * paginationDTO.RoundsPerPage)
                .Take(paginationDTO.RoundsPerPage);
        }
    }
}
