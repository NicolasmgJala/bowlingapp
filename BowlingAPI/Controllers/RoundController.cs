﻿using AutoMapper;
using BowlingAPI.DTOs;
using BowlingAPI.Entities;
using BowlingAPI.Helpers;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BowlingAPI.Controllers
{
    [ApiController]
    [Route("api/rounds")]
    public class RoundController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public RoundController(ApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        /*
        [HttpGet]
        public async Task<ActionResult<List<RoundDTO>>> Get()
        {
            var entities = await context.Rounds.ToListAsync();
            return mapper.Map<List<RoundDTO>>(entities);
        }*/
        [HttpGet]
        public async Task<ActionResult<List<RoundDTO>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = context.Rounds.AsQueryable();
            await HttpContext.PagesParameters(queryable, paginationDTO.RoundsPerPage);
            var entities = await queryable.Pagine(paginationDTO).ToListAsync();
            return mapper.Map<List<RoundDTO>>(entities);
        }

        [HttpGet("{id}", Name = "getRound")]
        public async Task<ActionResult<RoundDTO>> Get(int id)
        {
            var entity = await context.Rounds.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null) { return NotFound(); }

            return mapper.Map<RoundDTO>(entity);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CreateRoundDTO createRoundDTO)
        {
            var entity = mapper.Map<Round>(createRoundDTO);
            context.Add(entity);
            await context.SaveChangesAsync();
            var dto = mapper.Map<RoundDTO>(entity);
            return new CreatedAtRouteResult("getRound", new { id = entity.Id }, dto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] CreateRoundDTO createRoundDTO)
        {
            var entity = mapper.Map<Round>(createRoundDTO);
            entity.Id = id;
            context.Entry(entity).State = EntityState.Modified;
            return NoContent();

        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<RoundPatchDTO> patchDocument)
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            var entityDB = await context.Rounds.FirstOrDefaultAsync(x => x.Id == id);

            if (entityDB == null)
            {
                return NotFound();
            }

            var entityDTO = mapper.Map<RoundPatchDTO>(entityDB);
            patchDocument.ApplyTo(entityDTO, ModelState);
            var isValid = TryValidateModel(entityDTO);

            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            mapper.Map(entityDTO, entityDB);

            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var exist = await context.Rounds.AnyAsync(x => x.Id == id);
            if (!exist)
            {
                return NotFound();
            }

            context.Remove(new Round() { Id = id });
            await context.SaveChangesAsync();

            return NoContent();
        }


    }
}
