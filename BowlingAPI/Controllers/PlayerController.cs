﻿using AutoMapper;
using BowlingAPI.DTOs;
using BowlingAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BowlingAPI.Controllers
{
    [ApiController]
    [Route("api/players")]
    public class PlayerController: ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        public PlayerController(ApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<PlayerDTO>>> Get()
        {
            var entities = await context.Players.ToListAsync();
            var dtos = mapper.Map<List<PlayerDTO>>(entities);
            return dtos;
        }

        [HttpGet("{id:int}", Name = "getPlayer")]
        public async Task<ActionResult<PlayerDTO>> Get(int id)
        {
            var entity = await context.Players.FirstOrDefaultAsync(x => x.Id == id);
            if(entity == null)
            {
                return NotFound();
            }
            var dto = mapper.Map<PlayerDTO>(entity);
            return Ok(dto);
        }

        /*[HttpPost]
        public async Task<ActionResult> Post([FromBody] CreatePlayerDTO createPlayerDTO)
        {
            var entity = mapper.Map<Player>(createPlayerDTO);
            return Ok();
            context.Add(entity);
            await context.SaveChangesAsync();
            var playerDTO = mapper.Map<PlayerDTO>(entity);

            return new CreatedAtRouteResult("getPlayer", new { id = playerDTO.Id }, playerDTO);
        }*/

        [HttpPost]
        public async Task<ActionResult> Post([FromForm] CreatePlayerDTO createPlayerDTO)
        {
            var entity = mapper.Map<Player>(createPlayerDTO);
            context.Add(entity);
            await context.SaveChangesAsync();
            var playerDTO = mapper.Map<PlayerDTO>(entity);

            return new CreatedAtRouteResult("getPlayer", new { id = playerDTO.Id }, playerDTO);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] CreatePlayerDTO createPlayerDTO)
        {
            var entity = mapper.Map<Player>(createPlayerDTO);
            entity.Id = id;
            context.Entry(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var exist = await context.Players.AnyAsync(x => x.Id == id);
            if (!exist)
            {
                return NotFound();
            }

            context.Remove(new Player() { Id = id });
            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
