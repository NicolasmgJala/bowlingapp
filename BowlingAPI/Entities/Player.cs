﻿using System.ComponentModel.DataAnnotations;

namespace BowlingAPI.Entities
{
    public class Player
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength(40)]
        public string Nickname { get; set; }
        [Range(1, 300, ErrorMessage = "Out of range")]
        public int GlobalScore { get; set; }
        public List<Game> Games { get; set; }
    }
}
