﻿namespace BowlingAPI.Entities
{
    public class Game
    {
        public int PlayerId { get; set; }
        public int RoundId { get; set; }
        public int Order { get; set; }
        public DateTime Date { get; set; }

        public Player Player { get; set; }
        public Round Round { get; set; }

    }
}
