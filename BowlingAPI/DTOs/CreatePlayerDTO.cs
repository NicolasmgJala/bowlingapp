﻿using BowlingAPI.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BowlingAPI.DTOs
{
    public class CreatePlayerDTO
    {
        [Required]
        public string Name { get; set; }
        
        [StringLength(40)]
        public string Nickname { get; set; }
        public int GlobalScore { get; set; }
        
        [ModelBinder(BinderType = typeof(TypeBinder))]
        public List<int> RoundsIDs { get; set; }
    }
}
