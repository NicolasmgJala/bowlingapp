﻿using System.ComponentModel.DataAnnotations;

namespace BowlingAPI.DTOs
{
    public class CreateRoundDTO
    {
        [Range(1, 10, ErrorMessage = "Just ten rounds")]
        public int RoundNumber { get; set; }
        [Range(1, 2, ErrorMessage = "Just two shots")]
        public int ShootNumber { get; set; }
        [Range(1, 10, ErrorMessage = "Out of range")]
        public int Result { get; set; }
        [Range(1, 300, ErrorMessage = "Out of range")]
        public int GlobalScore { get; set; }
    }
}
