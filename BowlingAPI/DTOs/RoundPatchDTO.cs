﻿using System.ComponentModel.DataAnnotations;

namespace BowlingAPI.DTOs
{
    public class RoundPatchDTO
    {
        [Range(1, 10, ErrorMessage = "Out of range")]
        public int Result { get; set; }
        [Range(1, 300, ErrorMessage = "Out of range")]
        public int GlobalScore { get; set; }
    }
}
