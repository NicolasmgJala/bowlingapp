﻿using System.ComponentModel.DataAnnotations;

namespace BowlingAPI.DTOs
{
    public class PlayerDTO
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength(40)]
        public string Nickname { get; set; }
        public int GlobalScore { get; set; }
    }
}
