﻿namespace BowlingAPI.DTOs
{
    public class PaginationDTO
    {
        public int Page { get; set; } = 1;

        private int roundsPerPage = 10;
        private readonly int maxRoundPerPage = 50;

        public int RoundsPerPage
        {
            get => roundsPerPage;
            set
            {
                roundsPerPage = (value > maxRoundPerPage) ? maxRoundPerPage : value;
            }
        }
    }
}
